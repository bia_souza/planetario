# Exemplos do Nate Robbins

Para executar no Linux:

```
make transformation
make projection
make lightposition
make lightmaterial
make fog
make texture
```

Para limpar os arquivos executáveis e objeto:

```
make clean
```

Para usar a fucionalidade de carregamento de modelos obj, 
todo o código está em `glm.h` e `glm.c`.

