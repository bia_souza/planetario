#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "texture.c"

GLuint textura[10];
GLuint planetas[8];

float transladax = 0;
float transladay = 0;
int rotacionax = 0;
int rotacionay = 0;
int rotacionaz = 0;

int ativa_orbitas = 0;
int ativa_iluminacao = 1;
int camera = 

float zoom = -90;

GLuint orbitas = 0;
GLuint eixos = 0;


void iluminacao (float r, float g, float b)
{

	GLfloat ambiente[4]={r,g,b,1.0};
	GLfloat difusa[4]={r,g,b,1.0};
	GLfloat especular[4]={r, g, b, 1.0};
	GLfloat posicaoLuz[4]={0.0, 0.0, 0.0, 1.0}; //luz do sol, no centro
	GLfloat especularidade[4]={1.0,1.0,1.0,1.0};
	GLint especMat = 10;
	glMateriali(GL_FRONT_AND_BACK,GL_SHININESS,especMat);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambiente);
	glLightfv(GL_LIGHT0, GL_SPECULAR, especular );
	glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz );
	glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
}

void desenhaPlanetas(void)
{

	const double t = glutGet(GLUT_ELAPSED_TIME)*0.2/ 500.0;
	double a = t;

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslated(0, 0, zoom);
	glTranslated(transladax, 0, 0);
	glTranslated(0, transladay, 0);
	glRotated(-70, 1, 0, 0);
	glRotated(rotacionax, 1, 0, 0);
	glRotated(rotacionay, 0, 1, 0);
	glRotated(rotacionaz, 0, 0, 1);


	//sol
	GLUquadric*astro = gluNewQuadric();
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[0]);
	glPushMatrix();
	glRotated(30*a, 0, 0, 1);
	gluSphere(astro,1.39,25,25);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	int i;
	float angulo;

	if(ativa_orbitas == 1){
		glCallList(orbitas);
	}

	//mercurio
	glPushMatrix();
	glRasterPos2f(0,-1.9);
	glutBitmapString(GLUT_BITMAP_9_BY_15, "Mercurio");
	glTranslated(0, -1.9, 0);
	glTranslatef((2.3*cos(2.0*3.14*a*3.7/100)),(1.9 +1.9*sin(2.0*3.14*a*3.7/100)), 0);
	glRotated(-20*a, 0, 0, 1);
	glCallList(planetas[1]);
	glPopMatrix();


	//venus
	glPushMatrix();
	glRasterPos2f(0,-2.8);
	glutBitmapString(GLUT_BITMAP_9_BY_15, "Venus");
	glTranslated(0, -2.8, 0);
	glTranslatef((2.84*cos(2.0*3.14*a*2.5/100)),(2.8+ 2.8*sin(2.0*3.14*a*2.5/100)), 0);
	glRotated(-20*a, 0, 0, 1);
	glCallList(planetas[2]);
	glPopMatrix();


	//terra
	glPushMatrix();
	glRasterPos2f(0,-3.4);
	glutBitmapString(GLUT_BITMAP_9_BY_15, "Terra");
	glTranslated(0, -3.4, 0);
	glTranslatef((3.55*cos(2.0*3.14*a*1.9/100)),(3.4+3.4*sin(2.0*3.14*a*1.9/100)), 0);
	glRotated(-20*a, 0, 0, 1);
	glCallList(planetas[3]);
	glRotated(27, 1, 0, 0);
	glTranslatef(0.2*(cos(2.0*3.14*a*13/100)),(0.2*sin(2.0*3.14*a*13/100)), 0);
	glBindTexture(GL_TEXTURE_2D,textura[9]);
	gluSphere(astro,0.02,50,50);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();


	//marte
	glRasterPos2f(0,-4.2);
	glutBitmapString(GLUT_BITMAP_9_BY_15, "Marte");
	glPushMatrix();
	glTranslated(0, -4.2, 0);
	glTranslatef((4.51*cos(2.0*3.14*a*2.4/100)),(4.2 +4.2*sin(2.0*3.14*a*2.4/100)), 0);
	glRotated(-20*a, 0, 0, 1);
	glCallList(planetas[4]);
	glPopMatrix();


	//jupiter
	glPushMatrix();
	glRasterPos2f(0,-7.0);
	glutBitmapString(GLUT_BITMAP_9_BY_15, "Jupter");
	glTranslated(0, -7.0, 0 );
	glTranslatef((8.2*cos(2.0*3.14*a*1.3/100)),(7+7*sin(2.0*3.14*a*1.3/100)), 0);
	glRotated(-20*a, 0, 0, 1);
	glCallList(planetas[5]);
	glPopMatrix();
	

	//saturno
	glRasterPos2f(0,-11);
	glutBitmapString(GLUT_BITMAP_9_BY_15, "Saturno");
	glPushMatrix();
	glTranslated(0, -11, 0);
	glTranslatef((12.2*cos(2.0*3.14*a*0.9/100)),(11+11*sin(2.0*3.14*a*0.9/100)), 0);
	glRotated(27, 1, 0, 0);
	glRotated(-20*a, 0, 0, 1);
	glCallList(planetas[6]);
	

	//Lua de saturno
	glTranslatef((cos(2.0*3.14*a*10/100)),(sin(2.0*3.14*a*10/100)), 0);
	glutSolidSphere(0.05, 25,25);
	glPopMatrix();


	//urano
	glPushMatrix();
	glRasterPos2f(0,-20);
	glutBitmapString(GLUT_BITMAP_9_BY_15, "Urano");
	glTranslated(0, -20, 0);
	glTranslatef((22*cos(2.0*3.14*a*0.6/100)),(20+ 20*sin(2.0*3.14*a*0.6/100)), 0);
	glRotated(-20*a, 0, 0, 1);
	glCallList(planetas[7]);
	glPopMatrix();


	//netuno
	glPushMatrix();
	glRasterPos2f(0,-34);
	glutBitmapString(GLUT_BITMAP_9_BY_15, "Netuno");
	glTranslated(0, -34, 0);
	glTranslatef((37*cos(2.0*3.14*a*0.5/100)),(34+34*sin(2.0*3.14*a*0.5/100)), 0);
	glRotated(-20*a, 0, 0, 1);
	glCallList(planetas[8]);
	glPopMatrix();


	glutSwapBuffers();


}

void desenha(void)
{

	glDrawBuffer(GL_BACK);
       	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	desenhaPlanetas();
	glutSwapBuffers();

}

void inicializa (void)
{

	glEnable ( GL_TEXTURE_2D );
	glEnable(GL_COLOR_MATERIAL);
	glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );
	glGenTextures (10, textura);  
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	
	textura[0] = LoadBitmap("sol.bmp");
	textura[1] = LoadBitmap("mercurio.bmp");
	textura[2] = LoadBitmap("venus.bmp");
	textura[3] = LoadBitmap("terra.bmp");
	textura[4] = LoadBitmap("marte.bmp");
	textura[5] = LoadBitmap("jupter.bmp");
	textura[6] = LoadBitmap("saturno.bmp");
	textura[7] = LoadBitmap("urano.bmp");
	textura[8] = LoadBitmap("netuno.bmp");
	textura[9] = LoadBitmap("lua.bmp");

	if(ativa_iluminacao == 1){
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
	}
	else{
		glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
	}

	glEnable(GL_DEPTH_TEST);
	glClearColor(0.12f, 0.13f, 0.20f, 1.0f); //azul bem escuro
	defineGrafico();
}

void defineGrafico()
{

	int i;
	for( i = 1; i <9; i++)
		planetas[i] = glGenLists(i);

	orbitas = glGenLists(9);
	eixos = glGenLists(10);
	glNewList(eixos, GL_COMPILE);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3i(0, 0, 0);
	glVertex3i(200, 0, 0);
	glEnd();
	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3i(0, 0, 0);
	glVertex3i(0, 200, 0);
	glEnd();
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3i(0, 0, 0);
	glVertex3i(0, 0, 200);
	glEnd();
	glEndList();

	glNewList(planetas[1], GL_COMPILE);
	GLUquadric*astro = gluNewQuadric();
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[1]);
	gluSphere(astro,0.048,50,50);
	glDisable(GL_TEXTURE_2D);
	glEndList();
	glNewList(planetas[2], GL_COMPILE);
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[2]);
	gluSphere(astro,0.121,50,50);
	glDisable(GL_TEXTURE_2D);
	glEndList();
	glNewList(planetas[3], GL_COMPILE);
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[3]);
	gluSphere(astro,0.127,50,50);
	glDisable(GL_TEXTURE_2D);
	glEndList();
	glNewList(planetas[4], GL_COMPILE);
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[4]);
	gluSphere(astro,0.068,50,50);
	glDisable(GL_TEXTURE_2D);
	glEndList();
	glNewList(planetas[5], GL_COMPILE);
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[5]);
	gluSphere(astro,0.543,50,50);
	glDisable(GL_TEXTURE_2D);
	glEndList();
	glNewList(planetas[6], GL_COMPILE);
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[6]);
	gluSphere(astro,0.320,50,50);
	glDisable(GL_TEXTURE_2D);
	/*formula de iluminação baseada no cálculo de energia solar do site http://www.sunlab.com.br/dimensionamento_solar_fotovoltaico.htm*/
	glBegin(GL_LINE_LOOP);
		for(i = 0; i < 100+1; i++){
			iluminacao(0.86415094, 0.86328125, 0.8515625);
			glVertex2f(0.6*cos(2.0*3.14*i/100), 0.6*sin(2.0*3.14*i/100));
			glVertex2f(0.5*cos(2.0*3.14*i/100),0.5*sin(2.0*3.14*i/100));
			glColor3f(0.80078125f, 0.6875f,0.4296875f);
			glVertex2f(0.4*cos(2.0*3.14*i/100),0.4*sin(2.0*3.14*i/100));

		}
	glEnd();
	
	glEndList();
	glNewList(planetas[7], GL_COMPILE);
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[7]);
	gluSphere(astro,0.21,50,50);
	glDisable(GL_TEXTURE_2D);
	glEndList();
	glNewList(planetas[8], GL_COMPILE);
	gluQuadricTexture(astro,GL_TRUE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,textura[8]);
	gluSphere(astro,0.19,50,50);
	glDisable(GL_TEXTURE_2D);
	glEndList();

	//define as orbitas que os planetas possuem, os valores foram encontrados usando regra de 3 com as distâncias dos planetas
	glNewList(orbitas, GL_COMPILE);
	//mercurio
	glPushMatrix();
	glTranslated(0, -1.9, 0);
	iluminacao(0.6226415, 0.47265, 0.289062);
	glBegin(GL_LINE_LOOP);
		for (i = 0; i < 100+1; i++) {
			glVertex2f( 2.3*cos(2.0*3.14*i/100), 1.9+1.9*sin(2.0*3.14*i/100));
		}
	glEnd();
	glPopMatrix();
	//venus
	glPushMatrix();
	glTranslated(0, -2.8, 0);
	iluminacao(0.762264, 0.6875, 0.54296);
	glBegin(GL_LINE_LOOP);
		for (i = 0; i < 100+1; i++) {
			glVertex2f(2.84*cos(2.0*3.14*i/100), 2.8+2.8*sin(2.0*3.14*i/100));
		}
	glEnd();
	glPopMatrix();
	//terra
	glPushMatrix();
	glTranslated(0, -3.4, 0);
	iluminacao(0.09375,0.0859375, 0.33203125);
	glBegin(GL_LINE_LOOP);
		for (i = 0; i < 100+1; i++) {
			glVertex2f(3.55*cos(2.0*3.14*i/100),3.4+3.4*sin(2.0*3.14*i/100) );
		}
	glEnd();
	glPopMatrix();
	//marte
	glPushMatrix();
	glTranslated(0, -4.2, 0);
	iluminacao(0.83203125, 0.25390625, 0.12109375);
	glBegin(GL_LINE_LOOP);
	for (i = 0; i < 100+1; i++) {  
		glVertex2f(4.51*cos(2.0*3.14*i/100), 4.2+4.2*sin(2.0*3.14*i/100));
	}
	glEnd();
	glPopMatrix();

	//jupiter
	glPushMatrix();
	glTranslated(0, -7.0, 0 );
	iluminacao(0.86415094, 0.86328125, 0.8515625);
	glBegin(GL_LINE_LOOP);
	//glBegin(GL_POLYGON);
	for (i = 0; i < 100+1; i++) {  
		glVertex2f(8.2*cos(2.0*3.14*i/100),7+7*sin(2.0*3.14*i/100));
	}
	glEnd();
	glPopMatrix();
	//saturno
	glPushMatrix();
	glTranslated(0, -11, 0);
	iluminacao(0.80078125, 0.687, 0.429687);
	glBegin(GL_LINE_LOOP);
		for (i = 0; i < 100+1; i++) {  
			glVertex2f(12.2*cos(2.0*3.14*i/100),11+11*sin(2.0*3.14*i/100));
		}
	glEnd();
	glPopMatrix();
	//urano
	glPushMatrix();
	glTranslated(0, -20, 0);
	iluminacao(0.5742187, 0.644531, 0.730468);
	glBegin(GL_LINE_LOOP);
		for (i = 0; i < 100+1; i++) {  
			glVertex2f(22*cos(2.0*3.14*i/100),20+20*sin(2.0*3.14*i/100));
		}
	glEnd();
	glPopMatrix();
	//netuno
	glPushMatrix();
	glTranslated(0, -34, 0);
	glBegin(GL_LINE_LOOP);
		for (i = 0; i < 100+1; i++) {
			glVertex2f(37*cos(2.0*3.14*i/100),34+34*sin(2.0*3.14*i/100));
		}
	glEnd();
	glPopMatrix();
	glEndList();
}


void redimensiona(GLsizei w, GLsizei h)
{

	GLfloat b =(GLfloat) w/(GLfloat)h;
    	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	   /***PARA TRABALHARMOS EM 3D***/
                //glFrustum(-14, 14, -7.2, 7.2, 10, 800.0);

        gluPerspective (16, b, 1, 800);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Especifica posição do observador e do alvo
	gluLookAt(0,80,200, 50,-20,30, 0,1,0);
   	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void keyboard(unsigned char key, int x, int y)
{
   switch (key) {
	
	case 'o':
		if(ativa_orbitas == 1)
			ativa_orbitas = 0;
		else
			ativa_orbitas = 1;
		glutPostRedisplay();
	break;
	
	case 'i':
		if(ativa_iluminacao == 1)
			ativa_iluminacao = 0;
		else
			ativa_iluminacao = 1;
		glutPostRedisplay();
	break;

      case 27:
         exit(0);
	 break;
   }
}

void HandleMouse (int button, int state, int x, int y )
{
	if (button == GLUT_RIGHT_BUTTON)
		if (state == GLUT_UP)
	glutPostRedisplay();
}

void giraSistema(int key, int x, int y)
{
   switch (key) {
	
	case GLUT_KEY_UP:
		rotacionax +=2;
		glutPostRedisplay();
        break;
        
      	case GLUT_KEY_DOWN:
		rotacionax -=2;
		glutPostRedisplay();
	break;
	
	case GLUT_KEY_LEFT:
		rotacionay+=2;
		glutPostRedisplay();
	break;
	
	case GLUT_KEY_RIGHT:
		rotacionay-=2;
		glutPostRedisplay();
	break;
   }
	glutPostRedisplay();

}

static void idle(void)
{
    glutPostRedisplay();
    glutSwapBuffers();
}

int main(int argc, char**argv) {

     glutInit(&argc,argv);

     glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
     glutInitWindowSize(1200,720);
     glutInitWindowPosition(10,10);
     glutCreateWindow("Sistema Solar");
     glutDisplayFunc(desenha);
     glutIdleFunc(idle);
     glutMouseFunc(HandleMouse);
     glutReshapeFunc(redimensiona);
     inicializa();
     glutKeyboardFunc(keyboard);
     glutSpecialFunc(giraSistema);
     glutMainLoop();
}
